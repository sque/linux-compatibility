## Compatibility


| Operation | Ubuntu Gnome 16.04 64bit | 
| --------- | ------------------------ |
| **Power Management**    |                          |
|  Boot     |    ![PASS](/images/pass_32.png)     |
|  Halt     |    ![PASS](/images/pass_32.png)     |
|  Sleep    |    ![PASS](/images/pass_32.png)     |
| **Keyboard**|                          |
| Sleep BTN |    ![PASS](/images/pass_32.png)     |
| KB Diming BTN |  ![PASS](/images/pass_32.png) |
| Screen Diming BTN | ![PASS](/images/pass_32.png) |
| Volume Up/Down BTN | ![PASS](/images/pass_32.png) |
| Sound Mute BTN | ![PASS](/images/pass_32.png) |
| Wifi KillSwitch | ![PASS](/images/pass_32.png) [1] |
| Mic Mute BTN | ![FAIL](/images/fail_32.png) |
| NumLock BTN | ![PASS](/images/pass_32.png) |
| **Screen Lid** | |
|  Sleep on close | ![FAIL](/images/fail_32.png) |
|  Wakeup on close | ![FAIL](/images/fail_32.png) |
| **Mouse & TouchPad** | |
| Touch Area | ![PASS](/images/pass_32.png) |
| Touch Area MultiTouch | ![PASS](/images/pass_32.png) |
| Touch Mouse Buttons | ![PASS](/images/pass_32.png) |
| Pointing Stick | ![PASS](/images/pass_32.png) |
| Pointing Stick Buttons | ![PASS](/images/pass_32.png) |
| **Connectivity** | |
|  USB 3.1 | ![PASS](/images/pass_32.png) |
| USB type-c | ? not tested |
| DisplayPort | ![PASS](/images/pass_32.png) |
| HDMI        | N/A
| VGA         | ? not tested|
| WiFi        | ![PASS](/images/pass_32.png) |
| Bluetooth   | ? not tested |
| LAN         |  ![PASS](/images/pass_32.png) 
| DockPort    | ? not tested |
| Smart card | ? not tested |
| 3G         | ? not tested |
| Card Reader | ? not tested |
| EarPhones   | ![PASS](/images/pass_32.png) 
| EarPhones combo Mic | ![PASS](/images/pass_32.png) 
| **Sound** |
| **Peripherials** |
| FingerPrint Reader | ? not tested |
| WebCamera          |  ![PASS](/images/pass_32.png)
| 

[1] Button does not change color when kill switch is enabled.

## Hardware specs

### CPU

* CPU: `Intel(R) Core(TM) i5-6200U CPU @ 2.30GHz`

### Memory
#### Bottom-Slot 1(left):
```
	Total Width: 64 bits
	Data Width: 64 bits
	Size: 8192 MB
	Form Factor: SODIMM
	Bank Locator: ChannelA
	Type: DDR4
	Type Detail: Synchronous Unbuffered (Unregistered)
	Speed: 2133 MHz
	Manufacturer: Kingston
	Serial Number: 1C3FA65B
	Part Number: 9905668-002.A00G       
	Rank: 2
	Configured Clock Speed: 2133 MHz
	Configured Voltage: 1.2 V
```
#### Bottom-Slot 1(right):
```
	Total Width: 64 bits
	Data Width: 64 bits
	Size: 8192 MB
	Form Factor: SODIM
	Bank Locator: ChannelB
	Type: DDR4
	Type Detail: Synchronous Unbuffered (Unregistered)
	Speed: 2133 MHz
	Manufacturer: Kingston
	Serial Number: 193FA661
	Part Number: 9905668-002.A00G    
	Rank: 2
	Configured Clock Speed: 2133 MHz
	Configured Voltage: 1.2 V

```

### PCI Peripherials

```
00:00.0 Host bridge: Intel Corporation Sky Lake Host Bridge/DRAM Registers (rev 08)
00:02.0 VGA compatible controller: Intel Corporation Sky Lake Integrated Graphics (rev 07)
00:14.0 USB controller: Intel Corporation Sunrise Point-LP USB 3.0 xHCI Controller (rev 21)
00:14.2 Signal processing controller: Intel Corporation Sunrise Point-LP Thermal subsystem (rev 21)
00:15.0 Signal processing controller: Intel Corporation Sunrise Point-LP Serial IO I2C Controller (rev 21)
00:16.0 Communication controller: Intel Corporation Sunrise Point-LP CSME HECI (rev 21)
00:17.0 SATA controller: Intel Corporation Sunrise Point-LP SATA Controller [AHCI mode] (rev 21)
00:1c.0 PCI bridge: Intel Corporation Device 9d11 (rev f1)
00:1c.3 PCI bridge: Intel Corporation Device 9d13 (rev f1)
00:1f.0 ISA bridge: Intel Corporation Sunrise Point-LP LPC Controller (rev 21)
00:1f.2 Memory controller: Intel Corporation Sunrise Point-LP PMC (rev 21)
00:1f.3 Audio device: Intel Corporation Sunrise Point-LP HD Audio (rev 21)
00:1f.4 SMBus: Intel Corporation Sunrise Point-LP SMBus (rev 21)
00:1f.6 Ethernet controller: Intel Corporation Ethernet Connection I219-V (rev 21)
01:00.0 Unassigned class [ff00]: Realtek Semiconductor Co., Ltd. RTS522A PCI Express Card Reader (rev 01)
02:00.0 Network controller: Intel Corporation Wireless 8260 (rev 3a)
```
### USB Peripherials
```
Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 001 Device 004: ID 04ca:7054 Lite-On Technology Corp. 
Bus 001 Device 003: ID 138a:003f Validity Sensors, Inc. VFS495 Fingerprint Reader
Bus 001 Device 002: ID 8087:0a2b Intel Corp. 
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
```